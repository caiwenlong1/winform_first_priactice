﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("你好，夏天!");

            //MessageBox.Show("明天更美好", "标题");

            //MessageBox.Show("消息的主要内容", "消息标题", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Error);


            DialogResult dr = MessageBox.Show("你确定要退出吗？", "退出", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(dr ==DialogResult.Yes)
            { 
                Application.Exit();
             }
         }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
           
            BackColor = Color.Red;

          }

        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            BackColor = Color.Black;
        }
    }
}
